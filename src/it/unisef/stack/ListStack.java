package it.unisef.stack;

import java.util.ArrayList;
import java.util.List;

public class ListStack<T> implements StackInterface<T> {

	@Override
	public void push(T elem) throws FullStackException {
		_stack.add(elem);
		// TODO Auto-generated method stub

	}

	@Override
	public T pop() throws EmptyStackException {
		if (isEmpty()) {
			throw new EmptyStackException("Stack vuoto");
		}
			
		return _stack.remove(_stack.size() - 1);					
	}

	@Override
	public T top() throws EmptyStackException {
		if(isEmpty()) {
			throw new EmptyStackException("Stack vuoto");
		}
		
		return _stack.get(_stack.size() - 1);
	}

	@Override
	public boolean isEmpty() {
		return _stack.isEmpty();
	}

	@Override
	public boolean isFull() {
		// TODO Auto-generated method stub
		return false;
	}

	
	@Override
	public void printStack() {
		for(int i = 0; i < _stack.size(); i++)
			System.out.println(_stack.get(i));

	}

	
	public String toString() {
	/*
		StringBuilder sb = new StringBuilder(_stack.size() * 2);
		
		for (int i = 0; i <  _stack.size(); i++) {
			sb.append(_stack.get(i));
			sb.append("\n");
		}
		
		return sb.toString();
	*/	
		return _stack.toString();
				
	}
	
	public boolean equals(StackInterface<T> stack2) {
		// TODO Auto-generated method stub
	
		
		if(_stack.size() != stack2.size()) {
			return false;
		}
		
		for(int i = 0; i < _stack.size(); i++) {
			if(_stack.get(i) != stack2.get(i) ) {
				return false;
			}
		}
		
		return true;
	}
	
	
	public boolean equalsObject(Object object) {
		// TODO Auto-generated method stub
		
		ListStack<T> stack2 = (ListStack<T>)object;
		
		if(_stack.size() != stack2.size()) {
			return false;
		}
		
		for(int i = 0; i < _stack.size(); i++) {
			if(_stack.get(i) != stack2.get(i) ) {
				return false;
			}
		}
		
		return true;
	}
	
	
	private List<T> _stack = new ArrayList<T>();

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return _stack.size();
	}

	@Override
	public T get(int index) {
		// TODO Auto-generated method stub
		return _stack.get(index);
	}

	
}
