package it.unisef.stack;

public interface StackInterface<T> {

	public void push(T elem) throws FullStackException;
	
	public T pop() throws EmptyStackException;
	
	public T top() throws EmptyStackException;
	
	public boolean isEmpty();
	
	public boolean isFull();
	
	public String toString();
	
	public int size();
	
	public T get(int index);
	
	public boolean equalsObject(Object obj);
	
	public boolean equals(StackInterface<T> stack);
	
	public void printStack();

}
