package it.unisef.stack;

@SuppressWarnings("serial")
public class FullStackException extends Exception {

	public FullStackException() {
		super();
	}

	public FullStackException(String msg) {
		super(msg);
	}

	public FullStackException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public FullStackException(Throwable cause) {
		super(cause);
	}
}
