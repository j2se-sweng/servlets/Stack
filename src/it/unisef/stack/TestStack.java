package it.unisef.stack;

public class TestStack {

	public static void main(String[] args)  {
	
		StackInterface<String> stack = new ListStack<String>();
		StackInterface<Integer> stack2 = new ListStack<Integer>(); // NO tipi primitivi int, float, double, ecc..
		
		try {
				stack.push("trenta");
				stack.push("quindici");
				stack.push("uno");
				stack.push("quaranta");
				stack.push("venti");
					
				stack.pop();
				
				stack.printStack();
				
				stack.pop();
				stack.push("trecento");
						
				stack.printStack();
				
				stack.pop();
				stack.pop();
				stack.top();
				
				stack.printStack();
				stack.pop();
				stack.pop();
				//stack.pop();
				//stack.pop();
				stack.printStack();
				
				stack2.push(800);
				stack2.push(1200);
				stack2.push(1400);
				stack2.push(1600);
				stack2.push(1800);
				stack2.push(2000);
				stack2.push(new Integer(2200));
				
				stack2.printStack();
				stack2.pop();
				stack2.printStack();
				
				System.out.println("OK");
		}
		catch (EmptyStackException e) {
			System.out.println(e.toString());
		}
		catch (FullStackException e) {
			System.out.println(e.toString());
		}
		finally {
			System.out.println("Finally, la");
		}
		//System.out.println(e.toString());
		//System.out.println("Stack vuoto");
						
		System.out.println("fine.");
	}
	
}
